using System.ComponentModel.DataAnnotations;

namespace NetApiApp.Models.DTOs.Requests
{
    public class ProfileRequest
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        
    }
}