using System.Collections.Generic;

namespace NetApiApp.Models.DTOs.Responses
{
    public class DataResponses
    {
        public string Message { get; set; }
        public object Data { get; set; }
    }
}