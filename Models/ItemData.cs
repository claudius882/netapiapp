using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Http;

namespace NetApiApp.Models
{
    public class ItemData
    {
        public int Id { get; set; }

        public string User { get; set; }
        public string File { get; set; }
        [NotMapped]
        public IFormFile Image { get; set; }
    }
}