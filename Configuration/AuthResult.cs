using System.Collections.Generic;

namespace NetApiApp.Configuration
{
    public class AuthResult
    {
        public string Message { get; set; }
        public bool Success { get; set; }
        public List<string> Errors { get; set; }
        public string Token { get; set; }
    }
}