using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using NetApiApp.Data;
using NetApiApp.Models;
using NetApiApp.Models.DTOs.Responses;

namespace NetApiApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UploadController:ControllerBase
    {
        // private readonly ApiDbContext _context;
        private readonly ApiDbContext _context;
        private static IWebHostEnvironment _webHostEnvironment;

        public UploadController(IWebHostEnvironment webHostEnvironment, ApiDbContext context)
        {
            _webHostEnvironment=webHostEnvironment;
            _context=context;
        }

        [HttpPost]
        public async Task<IActionResult> Uploads([FromForm] ItemData obj)
        {
            
            if(obj.Image.Length>0 && obj.Image.ContentType.Contains("image")){
                try
                {
                    if(!Directory.Exists(_webHostEnvironment.WebRootPath + "/Images/"))
                    {
                        Directory.CreateDirectory(_webHostEnvironment.WebRootPath + "/Images/");
                    }
                    var rondomName = Guid.NewGuid()+"-"+ obj.Image.FileName;
                    using (FileStream  filestream = System.IO.File.Create(_webHostEnvironment.WebRootPath + "/Images/" + rondomName))
                    {
                        await obj.Image.CopyToAsync(filestream);
                        filestream.Flush();
                        var filePath="/Images/" + rondomName;
                        var userId=User.Claims.FirstOrDefault(x => x.Type == "Id").Value;
                        obj.User=userId;
                        obj.File=filePath;
                        await _context.Items.AddAsync(obj);
                        await _context.SaveChangesAsync();
                        var datas=new DataResponses(){
                            Message="Created success",
                            Data=obj
                        };
                        return Created("",datas);
                    }
                }catch(Exception e){
                    return BadRequest( new ApiResponses(){
                        Errors=new List<string>(){
                            e.ToString()
                        },
                        Success=false
                    });
                }
            }else{
                 return BadRequest( new ApiResponses(){
                    Errors=new List<string>(){
                        "invalid data"
                    },
                    Success=false
                });
            }
        }
    }
}