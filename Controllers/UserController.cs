using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NetApiApp.Models.DTOs.Requests;
using NetApiApp.Models.DTOs.Responses;

namespace NetApiApp.Controllers
{
    [Route("api/[controller]")] //api/auth
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UserController:ControllerBase
    {
        private readonly  UserManager<IdentityUser> _userManager;

        public UserController(UserManager<IdentityUser> userManager){
            _userManager=userManager;
        }

        [HttpGet]
        public async Task<IActionResult> GetUser()
        {
            var userId=User.Claims.FirstOrDefault(x => x.Type == "Id").Value;
            var existsUser=await _userManager.FindByIdAsync(userId);
            if(existsUser==null){
                return BadRequest( new ApiResponses(){
                    Errors=new List<string>(){
                        "User Not exists"
                    },
                    Success=false
                });
            }
            return Ok(new DataResponses(){
                Message="User Found",
                Data=existsUser,
            });
        }

        [HttpPost]
        public async Task<IActionResult>UpdateProfile([FromBody] ProfileRequest user)
        {
            if(ModelState.IsValid)
            {
                var userId=User.Claims.FirstOrDefault(x => x.Type == "Id").Value;
                var existsUser=await _userManager.FindByIdAsync(userId);
                if(existsUser==null) {
                    return BadRequest( new ApiResponses(){
                        Errors=new List<string>(){
                            "User Not exists"
                        },
                        Success=false
                    });
                }
                var existsEmail=await _userManager.FindByEmailAsync(user.Email);
                if(existsEmail!=null && existsEmail.Id!=userId) {
                    return BadRequest( new ApiResponses(){
                        Errors=new List<string>(){
                            "Email Already exists"
                        },
                        Success=false
                    });
                }
                var saveEmail=await _userManager.SetEmailAsync(existsUser,user.Email);
                if(!saveEmail.Succeeded){
                    return BadRequest( new ApiResponses(){
                        Errors=saveEmail.Errors.Select(x=>x.Description).ToList(),
                        Success=false
                    });
                }
                var saveUserName=await _userManager.SetUserNameAsync(existsUser,user.UserName);
                if(!saveUserName.Succeeded){
                    return BadRequest( new ApiResponses(){
                        Errors=saveUserName.Errors.Select(x=>x.Description).ToList(),
                        Success=false
                    });
                }
                var savePhone=await _userManager.SetPhoneNumberAsync(existsUser,user.PhoneNumber);
                if(!savePhone.Succeeded){
                    return BadRequest( new ApiResponses(){
                        Errors=savePhone.Errors.Select(x=>x.Description).ToList(),
                        Success=false
                    });
                }
                return Ok( new ApiResponses(){
                    Success=true,
                    Message="Update Success"
                });
            }
            return BadRequest( new ApiResponses(){
                Errors=new List<string>(){
                    "invalid payload"
                },
                Message="invalid payload",
                Success=false
            });
        }
    }
}