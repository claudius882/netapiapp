using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using NetApiApp.Configuration;
using NetApiApp.Models.DTOs.Requests;
using NetApiApp.Models.DTOs.Responses;

namespace NetApiApp.Controllers
{
    [Route("api/[controller]")] //api/auth
    [ApiController]
    public class AuthController:ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly JwtConfig _jwtConfig;

        public AuthController(
        UserManager<IdentityUser> userManager,
        IOptionsMonitor<JwtConfig> optionMonitor
        )
        {
            _userManager=userManager;
            _jwtConfig=optionMonitor.CurrentValue;
        }

        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register ([FromBody] RegistrationRequest user)
        {
            if(ModelState.IsValid)
            {
                var existsUser=await _userManager.FindByEmailAsync(user.Email);
                if(existsUser!=null){
                    return BadRequest( new ApiResponses(){
                        Errors=new List<string>(){
                            "Email Already exists"
                        },
                        Message="Email Already exists",
                        Success=false
                    });
                }
                var newUser=new IdentityUser(){
                    Email=user.Email,
                    UserName=user.UserName
                };
                var isCreated=await _userManager.CreateAsync(newUser,user.Password);
                if(isCreated.Succeeded){
                    var jwtToken=GenerateJwtToken(newUser);
                    return Ok( new ApiResponses(){
                        Token = jwtToken,
                        Success=true,
                        Message="Registration Success"
                    });
                } else{
                    return BadRequest( new ApiResponses(){
                        Errors=isCreated.Errors.Select(x=>x.Description).ToList(),
                        Success=false
                    });
                }
            }
            return BadRequest( new ApiResponses(){
                Errors=new List<string>(){
                    "invalid payload"
                },
                Message="invalid payload",
                Success=false
            });
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest user)
        {
            if(ModelState.IsValid){
                var existsUser=await _userManager.FindByEmailAsync(user.Email);
                if(existsUser==null){
                    return BadRequest( new ApiResponses(){
                        Errors=new List<string>(){
                            "Email Not exists"
                        },
                        Success=false
                    });
                }
                var isCorrect = await _userManager.CheckPasswordAsync(existsUser,user.Password);
                if(!isCorrect){
                    return BadRequest( new ApiResponses(){
                        Errors=new List<string>(){
                            "Wrong password"
                        },
                        Success=false
                    });
                }
                var jwtToken=GenerateJwtToken(existsUser);
                return Ok( new ApiResponses(){
                    Token = jwtToken,
                    Message="Login Success",
                    Success=true
                });
            }
            return BadRequest( new ApiResponses(){
                Errors=new List<string>(){
                    "invalid payload"
                },
                Message="invalid payload",
                Success=false
            });
        }
        private string GenerateJwtToken(IdentityUser user)
        {
            var jwtTokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtConfig.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject=new ClaimsIdentity(new []
                {
                    new Claim("Id",user.Id),
                    new Claim(JwtRegisteredClaimNames.Email,user.Email),
                    new Claim(JwtRegisteredClaimNames.Sub,user.Email),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                }),
                Expires=DateTime.UtcNow.AddDays(7),
                SigningCredentials= new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
            };
            var token=jwtTokenHandler.CreateToken(tokenDescriptor);
            var jwtToken=jwtTokenHandler.WriteToken(token);
            return jwtToken;
        }

    }
}