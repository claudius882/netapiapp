using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NetApiApp.Data;
using NetApiApp.Models;
using NetApiApp.Models.DTOs.Responses;

namespace NetApiApp.Controllers
{
    [Route("api/[controller]")]// api/item
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ItemController:ControllerBase
    {
        private readonly ApiDbContext _context;

        public ItemController(ApiDbContext context){
            _context=context;
        }

        [HttpGet]
        public async Task<IActionResult> GetItems(){
            var items= await _context.Items.ToListAsync();
            return Ok(new DataResponses(){
                Message="Data Found",
                Data=items
            });
        }

        [HttpPost]
        public async Task<IActionResult> CreateItems(ItemData data){
            if(ModelState.IsValid){
                var userId=User.Claims.FirstOrDefault(x => x.Type == "Id").Value;
                data.User= userId;
                await _context.Items.AddAsync(data);
                await _context.SaveChangesAsync();
                var datas=new DataResponses(){
                    Message="Created success",
                    Data=data
                };
                return Created("",datas);
            }
            return new JsonResult("Something went wrong") {StatusCode=500};
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetItem(int id){
            var item= await _context.Items.FirstOrDefaultAsync(x=>x.Id==id);
            if(item==null) {
                return BadRequest( new ApiResponses(){
                    Errors=new List<string>(){
                        "Data Not found"
                    },
                    Success=false
                });
            }
            return Ok(new DataResponses(){
                Message="Data Found",
                Data=item
            });
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateItem(int id,ItemData data){
            if(id!=data.Id){
                return BadRequest();
            }
            var existsItem=await _context.Items.FirstOrDefaultAsync(x=>x.Id==id);
            if(existsItem==null){
                return BadRequest( new ApiResponses(){
                    Errors=new List<string>(){
                        "Data Not found"
                    },
                    Success=false
                });
            }
            var userId=User.Claims.FirstOrDefault(x => x.Type == "Id").Value;
            existsItem.User=userId;
            existsItem.File=data.File;
            
            await _context.SaveChangesAsync();
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteItem(int id){
            var existsItem=await _context.Items.FirstOrDefaultAsync(x=>x.Id==id);
            if(existsItem==null){
                return BadRequest( new ApiResponses(){
                    Errors=new List<string>(){
                        "Data Not found"
                    },
                    Success=false
                });
            }
            _context.Items.Remove(existsItem);
            await _context.SaveChangesAsync();
            return Ok(new DataResponses(){
                Message="Data Deleted"
            });
        }

    }
}